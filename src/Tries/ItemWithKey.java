package Tries;

import java.util.ArrayList;

public interface ItemWithKey {

    //les clefs sont uniques, dont e1.getKey()==e2.getKey() <=> e1.equals(e2)
    //une clef peut être vide
    int getKeyLength(); //longueur de la clef de l'élément
    int getBase(); //retourne la base utilisée pour les chiffres de la clef
    int getDigit(int i); //pour 0 <= i < getKeyLength(), retourne le ième  chiffre d_i de la clef avec 0 <= d_i < getMaxDigit()

    public default ArrayList<Integer> getSubKey(int i){
        //pour 0 <= i <= getKeyLnegth() : donne la sous clefs de l'intervalle [0,..,i-1]
        ArrayList<Integer> res = new ArrayList<>();
        for(int j=0;j<i;j++){
            res.add(getDigit(j));
        }
        return res;
    }

    public default ArrayList<Integer> getKey(){
        return getSubKey(getKeyLength());
    }
}
